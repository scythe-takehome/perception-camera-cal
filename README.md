# Overview: 

This challenge consists of writing a program in C, C++, Python, or Rust that refines the intrinsic parameters of two cameras and the extrinsic transform between them given an initial guess for those parameters and synchronized images of corners on a calibration board. You are free to use whatever libraries you want.

You don’t need to spend a TON of time on this challenge. If you are spending 4+ hours on this feel free to send us the code where it is at and a short description of what you would have done if you had more time - see the What To Send Back section.

# Input: 

Your program will read the following from an input file:
* parameters describing the layout of the corners on the calibration board
* initial intrinsic and extrinsic parameter guesses
* observations of the calibration board corners in images

The calibration board consists of MxN corners in a rectangular layout. The horizontal and vertical spacing between the corners is equal. The first line of the input file contains the # of corners horizontally (N), # of corners vertically (M), and the spacing in meters. The format is:

* #_horizontally #_vertically spacing_in_meters

Each corner has a unique id. The ids start at 0 in the top left of the board and increase from left -> right, top -> bottom. The highest id is M*N-1 for the bottom right.

The second line of the input file contains the initial guess for the camera intrinsic parameters. The six parameter EUCM (enhanced unified camera model) model is used. The line format is:

* alpha beta fx fy cx cy

Refer to the following links if you aren’t familiar with the EUCM model:

[An Enhanced Unified Camera Model](https://hal.archives-ouvertes.fr/hal-01722264/document)  
[The Double Sphere Camera Model](https://arxiv.org/pdf/1807.08957.pdf)

The third line of the input file contains the initial guess for the extrinsic transform: cam0_from_cam1. The relation is point_in_cam0 = cam0_from_cam1 * point_in_cam1. The line format is:

* qw qx qy qz tx ty tz

Where, [qw; qx; qy; qz] is the rotation as a quaternion and [tx; ty; tz] is the translation.

The remaining lines of the input file describe the corner observations. The line format is:

* frame_id camera_id corner_id ix iy

frame_id is a unique integer for each synchronized pair of images (an image from cam0 and an image from cam1 both taken at the same instant in time). camera_id indicates the observing camera (0 or 1). corner_id indicates the observed corner of the calibration board (0 to M*N-1). (ix,iy) is the 2D image coordinate of the corner in the image.

Two example input files are available in the sample_inputs directory.

# Output:

Your program should find refined estimates for the EUCM model parameters of each camera (the two cameras can have different intrinsics), and the extrinsic transform between them. It should print the refined parameter estimates to stdout or save them to a file. It can be assumed that there are no incorrect corner to pixel correspondences in the input file. The pixel coordinates may be corrupted by gaussian noise on the order of 1 pixel stddev or less.

# What to send back:

1. Source code of your program and instructions for compiling and running it.
2. If you didn’t complete the challenge please include a brief description of the algorithmic approach you wanted to take and what parts of that are not yet implemented.

# How to send it back?

* Send us a .tar or .zip file containing all the above in the same thread where you scheduled all our interviews.

# If anything is unclear about what you need to do please reach out and we will clarify.
